"""
Exercise warmup2
-----------------

Simple exercises to get used to python basics (continued).

- From http://introtopython.org/lists_tuples.html , do the following:
  - first list
  - working list
  - starting from empty
  - ordered numbers (ignore the requirement of using for loops)
  - alphabet slices
  - first twenty

- And *also* the following exercise on dictionaries:
  Create the following dictionary to be used as a phonebook:

        phonebook = { "John" : 7566,  "Jack" : 7264,  "Jill" : 2781}

  Add "Jake" to the phonebook with the phone number 938273443,  and remove Jill
  from the phonebook. Then print the phonebook in alphabetical order
"""

# Write your solution here

# Write your solution here

# - first list

lista =['python', 'c', 'java']
lista[0]
lista[1]
lista[2]

# - working list

list2=['programmer', 'trucker','teacher','nurse']
list2.index('trucker')
print('trucker' in list2)
list2.append('blogger')
list2.insert(0,'youtuber')

# - starting from empty

list3=[]
list3.append('youtuber')
list3.append('programmer')
list3.append('trucker')
list3.append('teacher')
list3.append('nurse')
list3.append('blogger')
print(list3[0])
print(list3[-1])

# - ordered numbers

list4=[25,4,6,8,5]
print('Print the numbers in the original order:', list4)
print('Print the numbers in increasing order:', sorted(list4))
print('Print the numbers in the original order:', list4)
print('Print the numbers in decreasing order:' , sorted(list4, reverse=True))
print('Print the numbers in the original order:', list4)
print('Print the numbers in the reverse order from how they started', list4[::-1])
print('Print the numbers in the original order:', list4)
list4.sort()
print(list4)
list4.sort(reverse=True)
print(list4)


# - alphabet slices

list5=('a','b','c','d','e','f','g','h','i','j')
print(list5[:3])
print(list5[3:5])
print(list5[5:])

# - first twenty

first20=list(range(1,21))
print(first20)